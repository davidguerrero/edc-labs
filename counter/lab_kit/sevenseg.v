// Diseño: sevenseg
// Descripción: Codificador 7 segmentos
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

/*
       0             Segmentos activos en nivel bajo
      ---
   5 |   | 1
      --- 6
   4 |   | 2
      ---
       3
*/

`timescale 1ns / 1ps

module sevenseg(
    input wire [3:0] d,     // Número de entrada
    output reg [0:6] seg    // Código 7 segmentos de salida
    );

    always @*
        case (d)
            4'h0: seg = 7'b0000001;
            4'h1: seg = 7'b1001111;
            4'h2: seg = 7'b0010010;
            4'h3: seg = 7'b0000110;
            4'h4: seg = 7'b1001100;
            4'h5: seg = 7'b0100100;
            4'h6: seg = 7'b0100000;
            4'h7: seg = 7'b0001111;
            4'h8: seg = 7'b0000000;
            4'h9: seg = 7'b0000100;
            /* completar para un codificador hexadecimal */
            default: seg = 7'b1111110;    // entrada no válida
        endcase
endmodule    // sevenseg

/*
   Puede comprobar la sintáxis de este módulo con Icarus Verilog ejecutando
   en un terminal:

   $ iverilog sevenseg.v
 */
