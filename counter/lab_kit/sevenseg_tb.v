// Diseño: sevenseg
// Descripción: Codificador 7 segmentos. Banco de pruebas
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

`timescale 1ns / 1ps

// Tiempo base para facilitar la simulación
`define BTIME 10

module test();

    // Entradas
    reg [3:0] d;

    // Salidas
    wire [1:7] seg;

    // Instancia a simular
    sevenseg uut (.d(d), .seg(seg));

    // Iniciación de entradas y control de la simulación
    initial begin
        d = 0;

        // Directivas para generar formas de onda (opcional)
        $dumpfile("sevenseg_tb.vcd");
        $dumpvars(0,test);

        // Impresión de resultados
        $display("d\tseg");
        $monitor("%d\t%b", d, seg);

        // La simulación finaliza tras 16 ciclos
        #(16*`BTIME) $finish;
    end

    // Generación de dato de entrada
    always
        #(`BTIME) d = d + 1;

endmodule // test

/*
   Puede compilar y simular este diseño con Icarus Verilog ejecutando en
   un terminal:

   $ iverilog sevenseg.v sevenseg_tb.v
   $ vvp a.out

   Puede ver los resultados de simulación con el visor de ondas Gtkwave
   ejecutando en un terminal:

   $ gtkwave sevenseg_tb.vcd &
 */
