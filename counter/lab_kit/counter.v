// Diseño: counter
// Descripción: Contador ascendente módulo 16
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Escala de tiempo y resolución de la simulación
`timescale 1ns / 1ps

module counter(
    input wire clk,        // señal de reloj
    input wire clear,      // puesta a cero síncrona (activa en nivel alto)
    input wire up,         // cuenta ascendente (activa en nivel alto)
    output reg [3:0] q,    // estado de cuenta
    output wire cy         // fin de cuenta (carry)
    );

    /* introduce el código para el contador con estado q */


    /* introduce el código para la señal fin de cuenta
     * opción 1: "assign" condicional.
     * opción 2: procedimiento combinacional (cambia tipo de cy a reg) */


endmodule // counter

/*
   Puede comprobar la sintáxis de este módulo con Icarus Verilog ejecutando
   en un terminal:

   $ iverilog counter.v
 */
