// Diseño: code
// Descripción: Cerradura digital con cuatro botones y una salida.
//              Banco de pruebas
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 14-12-2011

`timescale 1ns / 1ps

// Banco de pruebas

module test();

    reg clk = 1;        // reloj
    reg reset = 0;      // puesta a cero
    reg [3:0] b;        // entrada
    wire z;             // salida

    // Instancia del módulo bajo test (Unit Under Test)
    code uut (.clk(clk), .reset(reset), .b(b), .z(z));

    initial begin
        // Generación de formas de onda
        $dumpfile("code_tb.vcd");
        $dumpvars(0, test);

        // Inicialización
        #5  reset = 1;
            b = 4'b0000;
        #10 reset = 0;
        #15 ;

        // Señales de entrada
        #20    b = 4'b0001;    // '0' pulsado
        #20    b = 4'b0001;
        #20    b = 4'b0001;
        #20    b = 4'b0000;    // '0' soltado
        #20    b = 4'b0000;
        #20    b = 4'b0100;    // '2' pulsado
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0000;    // '2' soltado
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b0010;    // '1' pulsado
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0000;    // '1' soltado (la cerradura debe abrir)
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b1000;    // '3' pulsado (la cerradura debe cerrar)
        #20    b = 4'b1000;
        #20    b = 4'b1000;
        #20    b = 4'b0000;    // '3' soltado
        #20    b = 4'b0000;
        #20    b = 4'b0100;    // algunos códigos incorrectos
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0000;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0000;
        #20    b = 4'b0000;

        #40    $finish;
    end

    // Generador de reloj
    always
        #10    clk = ~clk;
endmodule // test

/*
   Puede compilar y simular este diseño con Icarus Verilog ejecutando en
   un terminal:

   $ iverilog code.v code_tb.v
   $ vvp a.out

   Puede ver los resultados de simulación con el visor de ondas Gtkwave
   ejecutando en un terminal:

   $ gtkwave code_tb.vcd &
 */
