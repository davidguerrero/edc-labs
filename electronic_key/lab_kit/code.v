// Diseño: code
// Descripción: Cerradura digital con cuatro botones y una salida.
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 14-12-2011

/*
   Este archivo describe una cerradura digital simple. El sistema tiene
   cuatro entradas (b0, b1, b2 y b3) y una salida (z). La salida se activa (1)
   cuando las entradas se activan (1) en la secuencia: b0, b2, b1.

   La secuencia de pulsaciones correcta puede empezar en cualquier momento.
   Cuando la salida está activa (la cerradura está abierta) la pulsación de
   cualquier botón lleva al sistema al estado inicial en que se cierrar la
   cerradura.

   El sistema disponde de una señal de puesta a cero asíncrona activa en nivel
   alto que lleva al sistema al estado inicial.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Cerradura digital                                                    //
//////////////////////////////////////////////////////////////////////////

 module code(
    input wire clk,     // reloj
    input wire reset,   // puesta a cero
    input wire [3:0] b, // botones de entrada
    output reg z        // salida
    );

    // Codificación de estados (ejemplo)
    parameter [2:0]
        D1_PRESS = 3'b000,   // esperando pulsar primera cifra (b0)
        D1_REL   = 3'b001,   // esperando soltar primera cifra (b0)
        D2_PRESS = 3'b010,   // esperando pulsar segunda cifra (b2)
        D2_REL   = 3'b011,   // esperando soltar segunda cifra (b2)
        D3_PRESS = 3'b100,   // esperando pulsar tercera cifra (b1)
        D3_REL   = 3'b101,   // esperando soltar tercera cifra (b1)
        OPEN     = 3'b110;   // puerta abierta

    // Variables de estado y próximo estado
    reg [2:0] state, next_state;

    // Proceso de cambio de estado (secuencial)
    always @(posedge clk, posedge reset)
        if (reset)
            state <= D1_PRESS;
        else
            state <= next_state;

    // Cálculo del próximo estado (combinacional)
    always @* begin
        next_state = 3'bxxx;
        case (state)
        D1_PRESS:                       // esperamos que se pulse (press)
            if (b == 4'b0001)           // el botón 0
                next_state = D1_REL;
            else
                next_state = D1_PRESS;
        D1_REL:                         // esperamos que se suelte (release)
            if (b == 4'b0001)           // el botón 0
                next_state = D1_REL;
            else if (b == 4'b0000)
                next_state = D2_PRESS;
            else
                next_state = D1_PRESS;
        D2_PRESS:

            /* inserta aquí tu código */

        D2_REL:




            /* inserta aquí tu código */




        OPEN:

            /* inserta aquí tu código */

        endcase
    end

    // Proceso de cálculo de la salida (combinacional)
    always @* begin

        /* inserta aquí tu código */

    end
endmodule // code
