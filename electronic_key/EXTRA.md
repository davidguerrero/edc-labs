Diseño de una cerradura digital. Mejoras del sistema
====================================================

**Estructura de Computadores.**

**Ing. Informática. Tecnologías Informáticas.**

**ETSI Informática. Sevilla**

## Material

- Ordenador con el entorno [ISE de Xilinx][ise] instalado.
- Placa de desarrollo FPGA Digilent Basys 2 y [documentación][basys2].
- Archivos base del diseño (kit de la práctica).
- Servomotor y cables de conexión (opcional).   

[ise]: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/design-tools/v2012_4---14_7.html
[basys2]: https://store.digilentinc.com/basys-2-spartan-3e-fpga-trainer-board-limited-time/

## Filtros de rebotes

Los pulsadores mecánicos pueden producir *rebotes*. Este efecto consiste en que
el pulsador conecta y desconecta sus terminales varias veces de forma muy rápida
al ser pulsado, por lo que la entrada asociada a un pulsador generará una
secuencia de bits como `00000010110111111` y no el esperado `0000000111111111`.

¿Incorpora la placa Basys 2 dispositivos de filtrado de rebotes en sus botones?
Busca en la [documentación de la placa Basys 2][basys2] información del
fabricante sobre si la placa incluye algún mecanismo de filtrado de rebotes.

Cuando la señal de entrada original tiene rebotes, debe incluirse un módulo de
filtro de rebotes en la entrada de cada botón, el cual puede combinarse con un
detector de flancos tal como se ve en la figura siguiente, donde se muestra una
señal del tipo de las que genera un botón al pulsarse y que, tras pasar por un
filtro de rebotes y un detector de flanco, genera un pulso único fácil de
gestionar por el circuito principal (máquina de estados, etc.).

![Filtro de rebotes y detector de flanco](images/debouncer_edge.png)

Crea un un nuevo proyecto (ej. *code_db*) tomando como partida los archivos
de la versión con detectores de flanco, prueba lo siguiente:

1.  Introduce la clave correcta en el diseño implementado. Oserva si la clave
    desbloquea la cerradura en todos los casos o falla a veces. Si es así, es
    probable que el filtrado de rebotes incluido en la placa de desarrollo no
    sea lo suficientemente efectivo.

1.  Modifica el diseño para incluir un filtro de rebotes en cada entrada, antes
    de cada detector de flancos, como aparece en las figuras anteriores. En el
    archivo `debouncer.v` se encuentra ya diseñado un filtro de rebotes (módulo
    *debouncer*).

1.  Implementa y prueba de nuevo el diseño. ¿Falla la clave
    correcta menos que antes?

## Control de una barrera

Ya que tenemos diseñado el módulo de cerradura electrónica (cualquiera de
sus versiones) podemos aplicarlo a un caso práctico: usar la salida del
sistema para abrir y cerrar una barrera.

Una forma de mover puertas y barreras de forma automática es mediante el uso de
servomotores. Este tipo de motores son controlados mediante pulsos eléctricos y
pueden ser desplazados a una posición controlando la anchura de los pulsos. Si
unimos una barrera al eje de un servo motor, podemos desplazarla fácilmente de
una posición horizontal (cerrado) a un vertical (abierto) generando las señales
de control apropiadas para el servomotor.

El kit de la práctica incluye un archivo *servo_gate.v* que contiene el módulo
**servo_gate**. Este módulo  es un controlador de servomotores que permite
moverlos desde un extremo a otro de su rango, que normalmente son 90 grados. La
interfaz del módulo es la  siguiente:

```verilog
module servo_gate #(
    parameter FS = 16000000,        // clock frequency (Hz)
    )(
    input wire clk,                 // system clock
    input wire gate,                // servo angle (0-minimum, 1-maximum)
    output wire servo,              // output signal
    output wire finished            // target angle reached
);
```

El módulo se conecta a la entrada de control del servo motor mediante la salida
**servo**. Cuando la entrada **gate** vale 0, el servomotor se moverá a su
ángulo mínimo, y cuando vale 1 se moverá al ángulo máximo. De esta forma,
podemos conectar la salida **z** de nuestra cerradura electrónica a la entrada
del módulo de control del servomotor y accionar una barrera real.

El objetivo de este apartado es crear un sistema descrito en Verilog que
consiste en la unión del módulo de cerradura digital (*code*) y el controlador
del servomotor (*servo_gate*). De esta forma, al introducir el código correcto
en la cerradura, está activará la apertura de una barrera real, accionada por el
servomotor. El esquema es el siguiente:

![Control de barrera con servo](images/code_servo_inside.png)

El procedimiento general para construir el sistema es es el siguiente:

1. Dibuja previamente el sistema sobre el papel indicando los módulos que
   conecta y poniendo nombre a todas las señales de entrada y salida del
   sistema, de los módulos que contiene y sus señales de interconexión. Las
   señales del sistema deben ser:

   Entradas:
   * **clk**: reloj del sistema.
   * **reset**: puesta a cero del sistema.
   * **b[3:0]**: botones de entrada.

   Salidas:

   * **servo**: señal de control del servomotor

1.  Crea un nuevo proyecto (ej. *code_servo*) y copia los archivos Verilog del
    proyecto *code_ed* anterior.

1.  En un nuevo archivo del proyecto, realiza la descripción en Verilog del
    sistema dibujado que consistirá simplemente en la correcta conexión de los
    módulos *code* y *servo_gate*. Al instanciar el módulo *servo_gate* tenga
    en cuenta que debe redefinir el parámetro **FS** al valor de la frecuencia
    de reloj usado por la placa Basys2 (50000000Hz). Por ejemplo:

    ```verilog
    servo_gate servo_ctrl #(.FS(50000000))
        (.clk(clk), .gate(z), .servo(servo));
    ```

1.  Comprueba la sintáxis del código. No es necesario hacer un banco de pruebas
    por el momento.

1.  Prepara un archivo UCF para el sistema. Puedes basarte en el archivo para
    la cerradura digital, solo que ahora la salida será la señal *servo* del
    sistema que deberá conectarse a uno de los pines de los conectores de
    expansión de la placa. Para facilitar la conexión del servomotor puedes
    usar el pin JA4 del conector JA. Búscalo en el UCF y configúralo con la
    señal correspondiente. Debe quedar algo como:

        NET "servo" LOC = "B5" ; # Bank = 1, Signal name = JA4

    Observa que se han eliminado las opciones *DRIVE* y *PULLUP* que aparecen
    en el archivo original ya que estas opciones no se usan cuando el pin se
    configura como salida.

    Las conexiones del módulo completo son las siguientes:

    ![Conexión módulo code_servo](images/code_servo.png)

1.  Monta en el eje del servo alguno de los brazos que trae, pero no lo fijes
    con el tornillo hasta que determinemos cual va a ser su posición
    definitiva.

1.  Los servos usan tres cables de conexión: polarización negativa (GND),
    polarización positiva (VCC) y señal de control. Localiza qué cable del servo
    hace cada función. Si no queda claro, mira el código del dispositivo y
    busca su ficha técnica en Internet. Luego conéctalo a la placa usando un
    juego de cables adecuado de la siguiente forma:

    * Polarización positiva: pin VCC del conector JA.
    * Polarización negativa: pin GND del conector JA.
    * Señal de control: pin JA4 del conector JA.

1.  Sintetiza el diseño en la placa y comprueba su operación. Al introducir el
    código correcto en la cerradura, el servomotor debe girar 90 grados. Si no
    fuera así, revisa las conexiones del servo con la placa y las conexiones
    entre los módulos en el código Verilog del sistema. Si no consigues que
    funcione el sistema tendrás que hacer un banco de pruebas y simularlo.
    Usa el banco de pruebas del módulo *code* como referencia.

1.  Una vez que el sistema esté funcionando, monta el servo sobre algún
    soporte, ajusta el brazo del servo para que esté horizontal cuando la
    barrera esté cerrada y vertical cuando esté abierta; y monta sobre el
    brazo una tira de cartón o madera. Tendrás un bonito prototipo de una
    barrera accionada mediante una cerradura digital.
