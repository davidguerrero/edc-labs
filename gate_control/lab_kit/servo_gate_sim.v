// Modules: servo, servo_soft, servo_gate
// Description: Various servo controllers including a servo-based gate cont.
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 2019-01-18

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

//
// SERVO GATE SIMULATION MODEL
//

/*
   One-bit speed-configurable servo controller using the "servo_soft" module.
   Set the servo angle to the minimum or maximum value depending on one bit
   input.

   Input/output signals

     * clk: system clock (this is a synchronous design)
     * gate: gate position.
           0 - closed (set angle to minimum value).
           1 - open (set angle to maximum value).
     * servo: output control signal. Connect to servo control input.
     * finished: Servo has reached the target angle.

   Parameters

   It uses the same set of parameters than the soft servo driver. Parameter
   values are passed to the soft servo driver. See soft servo driver code for
   details.

   System clock frequency (FS) must be set in all cases. The rest have typical
   defaults for most servos.

     * FS: system clock frequency in Hz (defaults to 16MHz). ALWAYS SET TO
       SYSTEM'S FREQUENCY!
     * DELAY: time spent to cover the whole range of the servo. In seconds
       (integer). Minimum value is 1.
 */

// Xilinx ISE does not like big numbers in params hence us
`timescale 1us / 1ps

module servo_gate #(
    parameter FS = 100,             // clock frequency (Hz)
    parameter DELAY = 4             // whole range delay (s)
    )(
    input wire clk,                 // system clock
    input wire gate,                // servo angle (0-minimum, 1-maximum)
    output wire servo,              // output signal
    output reg finished = 1         // target angle reached
    );

    localparam DELAY_U = 10**6 * DELAY;

    reg gate_pre = 0;

    always @(clk)
        if(gate != gate_pre) begin
            gate_pre = gate;
            finished = 1'b0;
            #DELAY_U finished = 1'b1;
        end

    assign servo = gate;
endmodule
