// Módulo: system
// Descripción: Control de barrera con temporizador
// Proyecto: Sistema de control de una barrera
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Fecha original: 21-02-2019

module system #(
    parameter SYS_FREQ = 50000000   // frecuencia de reloj del sistema (Hz)
    )(
    input wire clk,         // reloj del sistema
    input wire reset,       // puesta a cero del sistema
    input wire open,        // señal de apertura (activa en alto)
    output wire gate,       // salida de control (0-cerrar, 1-abrir)
    output wire servo       // salida de control del servo
    );

    wire eoc;       // fin de cuenta del temporizador
    wire clear;     // puesta a cero del contador
    wire enable;    // habilitación del temporizador

    /*** instancia y conecta tres módulos: control, timer, servo_gate ***/

    servo_gate #(
        .FS(SYS_FREQ)
        ) servo_gate (
        .clk(clk),
        .gate(gate),
        .servo(servo)
        // .finished(finished) // sin conectar (todavía)
        );


endmodule // system
