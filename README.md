Prácticas de laboratorio de Estructura de Computadores
======================================================

**[Departamento de Tecnología Electrónica](http://www.dte.us.es)**

**Ing. Informática. Tecnologías Informáticas.**

**ETSI Informática. Sevilla**

Listado de prácticas
--------------------

* [Práctica 1](counter/): Contador con salida en código 7 segmentos.

* [Práctica 2](electronic_key/): Cerradura electrónica.

* [Práctica 3](gate_control/): Control de una barrera.
